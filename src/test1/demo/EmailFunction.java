package test1.demo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import test1.demo.mail.MailBase;

@SuppressWarnings("restriction")
public class EmailFunction {

	/**
	 * 发送邮件的props文件 (可使用自建priperty文件) 用于初始化一个session实例，配置了一个session会话的一些基本信息
	 */
	private final transient Properties props = new Properties();

	/**
	 * 邮件发送者的用户名和密码
	 */
	private transient String username = "wpeng@rgprt.com";
	private transient String password = "wzbhbb67782155";

	/**
	 * session为一个基本的邮件会话，通过该会话可执行其他邮件工作 如：之后的初始化一个MimeMessage实例
	 */
	private transient Session session;

	/**
	 * MIME类型邮件MimeMessage类（抽象消息类Message的一个子类）
	 * 可以通过将Session对象传递给MimeMessage构造器的方法来创建
	 */
	private transient MimeMessage message;

	/**
	 * 邮件内容类型 （这里演示一个html格式的消息格式）
	 */
	private final static String CONTENT_TYPE_HTML = "text/html;charset=utf-8";

	/**
	 * 端口号
	 */
	private final static int MAIL_PORT = 465;

	/**
	 * 邮件内容
	 */
	private String content = "点击进入» <a href='http://www.cnblogs.com/liuyitian'>刘一天的博客</a>";

	/**
	 * 继承Authenticator子类用于用户认证 （这里指邮件服务器对用户的认证）
	 * 也可外部创建一个单独的邮件实体类（包涵用户名/密码即可），继承Authenticator类来重写PasswordAuthentication方法
	 */
	static class MyAuthenricator extends Authenticator {
		private String user = null;
		private String pass = "";

		public MyAuthenricator(String user, String pass) {
			this.user = user;
			this.pass = pass;
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(user, pass);
		}

	}

	/**
	 * 初始化 session 实例方法
	 * 
	 * @param username
	 *            发送邮件的用户名(地址)
	 * @param password
	 *            密码
	 * @param smtpHostName
	 *            SMTP邮件服务器地址
	 */
	private void initSession(String username, String password, String smtpHostName) {

		// 初始化props文件
		props.setProperty("mail.transport.protocol", "smtp");// 发送邮件协议
		props.put("mail.smtp.auth", "true"); // 需要验证
		props.put("mail.smtp.host", smtpHostName); // 服务器地址

		// 根据property文件创建session,并传入Authenticator进行验证
		session = Session.getInstance(props, new MyAuthenricator(username, password));

		// 是否控制台打印消息列表 （可选）
		session.setDebug(true);
	}

	/**
	 * 初始化邮箱message（消息实例）方法
	 * 
	 * @param subject
	 *            邮件主题
	 * @param content
	 *            邮件内容
	 * @throws MessagingException
	 * @throws AddressException
	 * @throws UnsupportedEncodingException
	 */
	private void initMessage(String subject, Object content)
			throws AddressException, MessagingException, UnsupportedEncodingException {

		// 根据session创建一个消息对象
		message = new MimeMessage(session);

		// 设置发件人地址 (第二个参数为显示发件人名称，目前还没有测试成功)
		message.setFrom(new InternetAddress(username, "java 测试"));

		// 设置邮件的主题
		message.setSubject("主题：javamail测试邮件");

		// 设置邮件的发送内容和内容的content-type（这里采用text/html格式）
		message.setContent(content.toString(), EmailFunction.CONTENT_TYPE_HTML);

		// 设置邮件的接收人地址--方法一 (选其一即可)
		// Address[] address = new Address[]{new
		// InternetAddress("418874847@qq.com"),"...更多列表..."};
		// message.setReplyTo(addresses);

		// 设置邮件的接收人地址--方法二 (选其一即可)
		// 如果群发邮件，收件人较多，可另写一个方法用于专门循环遍历并设置接收人
		message.addRecipient(Message.RecipientType.TO, new InternetAddress("wzbhbb@qq.com"));
//		message.addRecipient(Message.RecipientType.TO, new InternetAddress("12450374@qq.com"));
	}

	/**
	 * 初始化邮件发送器方法
	 * 
	 * @param username
	 *            发送邮件的用户名(地址)，并以此解析SMTP服务器地址
	 * @param password
	 *            发送邮件的密码
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 * @throws AddressException
	 * @return
	 */
	public void SimpleMailSender(final String username, final String password)
			throws AddressException, UnsupportedEncodingException, MessagingException {

		// 通过邮箱地址解析出smtp服务器，对大多数邮箱都管用 (还有IMAP和POP3)
//		final String smtpHostName = "smtp." + username.split("@")[1];
		final String smtpHostName = "smtp.exmail.qq.com";

		// 调用初始化session方法
		initSession(username, password, smtpHostName);

		// 调用初始化MimeMessage方法 （在初始化session完毕后）
		initMessage("邮件主题：测试邮件", content);
	}

	/**
	 * 邮件发送
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("unused")
	private void send() throws MessagingException, UnsupportedEncodingException {

		// 调用初始化邮件方法
		SimpleMailSender(username, password);

		// 根据session来获得一个Transport抽象类对象
		Transport tran = session.getTransport();

		// 打开链接 ,此时会去校验用户名和密码 (参数列表：【 邮箱服务器地址】【端口号】【 发件箱用户名】【发件箱密码】)
		tran.connect(props.getProperty("mail.smtp.host"), EmailFunction.MAIL_PORT, username, password);

		// 发送邮件 (第二个参数null指收件人地址，因为在初始化message时已经设置好了收件人地址，此处便省略)
		tran.sendMessage(message, null);

		// 关闭通道
		tran.close();
	}

	@SuppressWarnings("restriction")
	public static void SendMail() {
        try {
            //设置SSL连接、邮件环境
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());  
            final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";  
            Properties props = System.getProperties();
            props.setProperty("mail.smtp.host", "smtp.exmail.qq.com");
            props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.port", "465");
            props.setProperty("mail.smtp.socketFactory.port", "465");
            props.setProperty("mail.smtp.auth", "true");
            //建立邮件会话
            Session session = Session.getDefaultInstance(props, new Authenticator() {
                //身份认证
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("wpeng@rgprt.com", "wzbhbb67782155");
                }
            });
            //建立邮件对象
            MimeMessage message = new MimeMessage(session);
            //设置邮件的发件人、收件人、主题
                //附带发件人名字
//            message.setFrom(new InternetAddress("from_mail@qq.com", "optional-personal"));
            message.setFrom(new InternetAddress("wpeng@rgprt.com"));
            message.setRecipients(Message.RecipientType.TO, "wzbhbb@qq.com");
            message.setSubject("通过javamail发出！！！");
            //文本部分
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setContent("图<img src='cid:myimg'/>文加附件邮件测试", "text/html;charset=UTF-8");
//            //内嵌图片部分
//            MimeBodyPart imagePart = new MimeBodyPart();
//            imagePart.setDataHandler(new DataHandler(new FileDataSource("C:\\Users\\wlp\\Desktop\\1.png")));//图片路径
//            imagePart.setContentID("myimg");
            //图文整合，关联关系
            MimeMultipart mmp1 = new MimeMultipart();
            mmp1.addBodyPart(textPart);
//            mmp1.addBodyPart(imagePart);
            mmp1.setSubType("related");
            MimeBodyPart textImagePart = new MimeBodyPart();
            textImagePart.setContent(mmp1);
            //附件部分
            MimeBodyPart attachmentPart = new MimeBodyPart();
            DataHandler dh = new DataHandler(new FileDataSource("C:\\Users\\wlp\\Desktop\\2.pdf"));//文件路径
            String fileName = dh.getName();
            attachmentPart.setDataHandler(dh);
            attachmentPart.setFileName(fileName);
            //图文和附件整合，复杂关系
            MimeMultipart mmp2 = new MimeMultipart();
            mmp2.addBodyPart(textImagePart);
            mmp2.addBodyPart(attachmentPart);
            mmp2.setSubType("mixed");
            //将以上内容添加到邮件的内容中并确认
            message.setContent(mmp2);
            message.saveChanges();
            //发送邮件
            Transport.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

	}
	
    /**    
     * 对复杂邮件的解析    
     * @param multipart    
     * @throws MessagingException    
     * @throws IOException    
     */      
	public static void parseMultipart(Multipart multipart) throws MessagingException, IOException {      
        int count = multipart.getCount();      
        System.out.println("couont =  "+count);      
        for (int idx=0;idx<count;idx++) {      
            BodyPart bodyPart = multipart.getBodyPart(idx);      
            System.out.println(bodyPart.getContentType());      
            if (bodyPart.isMimeType("text/plain")) {      
                System.out.println("plain................."+bodyPart.getContent());      
            } else if(bodyPart.isMimeType("text/html")) {      
                System.out.println("html..................."+bodyPart.getContent());      
            } else if(bodyPart.isMimeType("multipart/*")) {      
                Multipart mpart = (Multipart)bodyPart.getContent();      
                parseMultipart(mpart);      
                      
            } else if (bodyPart.isMimeType("application/octet-stream")) {      
                String disposition = bodyPart.getDisposition();      
                System.out.println("发现一个位置:"+ disposition);      
                if (disposition.equalsIgnoreCase(BodyPart.ATTACHMENT)) {     
                    try { 
                    	//=?GB2312?B?1tzA/bvhvMfCvDIwMTQuNC4xNC5kb2M=?=
	                    String fullFileName = bodyPart.getFileName();     
	                    
	                    //分离编码方式
	                    String []paras = fullFileName.split("\\?");//1存放编码方式， 3存放名称	          
						String fn = new String(Base64.decode(paras[3].getBytes(paras[1])), paras[1]);
	                    System.out.println("发现一个附件路径: " + fn);
	                    InputStream is = bodyPart.getInputStream();      
	                    copy(is, new FileOutputStream("E:\\CloudMusic\\"+ fn));  
					} catch (Base64DecodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}    
                }      
            }      
        }      
    }      
    
    /**      
     * 文件拷贝，在用户进行附件下载的时候，可以把附件的InputStream传给用户进行下载      
     * @param is      
     * @param os      
     * @throws IOException      
     */      
    public static void copy(InputStream is, OutputStream os) throws IOException {      
        byte[] bytes = new byte[1024];      
        int len = 0;      
        while ((len=is.read(bytes)) != -1 ) {      
            os.write(bytes, 0, len);      
        }      
        if (os != null)      
            os.close();      
        if (is != null)      
            is.close();      
    }  
    
	/**
	 * POP3方式读取邮件
	 */
	public static void FetchMailPop3() {
        //设置SSL连接、邮件环境
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        Properties props = System.getProperties();
        props.setProperty("mail.pop3.host", "pop.exmail.qq.com");
        props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.pop3.socketFactory.fallback", "false");
        props.setProperty("mail.pop3.port", "995");
        props.setProperty("mail.pop3.socketFactory.port", "995");
        props.setProperty("mail.pop3.auth", "true");
        //建立邮件会话
        Session session = Session.getDefaultInstance(props, null);

        System.out.println("step 1");
//        session.setDebug(true);  
        //设置连接邮件仓库的环境
        URLName url = new URLName("pop3", "pop.exmail.qq.com", 995, null, "wpeng@rgprt.com", "wzbhbb67782155");
        Store store = null;
        Folder inbox = null;
        try {
            //得到邮件仓库并连接
            store = session.getStore(url);
            store.connect();
            //得到收件箱并抓取邮件
            inbox = store.getFolder("INBOX");  
            inbox.open(Folder.READ_WRITE); //可以删除邮件

            int messageCount = inbox.getMessageCount();      
            System.out.println(messageCount);      
            Message[] messages = inbox.getMessages();    
            
            //打印收件箱邮件部分信息
            int length = messages.length;
            System.out.println("收件箱的邮件数：" + length);
            System.out.println("-------------------------------------------\n");
            for (int i = 0; i < length; i++) {
                String from = MimeUtility.decodeText(messages[i].getFrom()[0].toString());
                InternetAddress ia = new InternetAddress(from);
                System.out.println("发件人：" + ia.getPersonal() + '(' + ia.getAddress() + ')');
                System.out.println("主题：" + messages[i].getSubject());
                System.out.println("邮件大小：" + messages[i].getSize());
                System.out.println("邮件发送时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(messages[i].getSentDate()));
                
                if("Re:Re: (Zhaopin.com) 应聘 软件工程师-北京-李振鹏".equals(messages[i].getSubject())) {
                	System.out.println("++++++++++++++++++++++++++++++找到了+++++++++++++++++++======");
                	messages[i].setFlag(Flags.Flag.DELETED, true);
                	break;
                }
                // 解析邮件内容
                Object content = messages[i].getContent(); 
                if (content instanceof MimeMultipart) {      
                    MimeMultipart multipart = (MimeMultipart) content;      
                    parseMultipart(multipart);      
                } 
                System.out.println("-------------------------------------------\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                inbox.close(true);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            try {
                store.close();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
	}
	
	/**
	 * 通过IMAP方式读取邮件
	 */
	public static void FetchMailImap() {
		String host = "imap.exmail.qq.com";
		int port = 993;
		String username = "wpeng@rgprt.com";
		String password = "wzbhbb67782155";
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		Properties props = System.getProperties();
		props.setProperty("mail.imap.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.imap.socketFactory.fallback", "false");
		props.setProperty("mail.imap.port", "993");
		props.setProperty("mail.imap.socketFactory.port", "993");
		props.setProperty("mail.imap.ssl.enable", "true");
		props.setProperty("mail.store.protocol", "imap");
		props.setProperty("mail.imap.host", host);
		props.setProperty("mail.imap.auth.login.disable", "true");
		Session session = Session.getInstance(props);
		session.setDebug(false);
		IMAPFolder folder = null;
		IMAPStore store = null;
		try {
			store = (IMAPStore) session.getStore("imap"); // 使用imap会话机制，连接服务器
			store.connect(username, password);
			// folder=(IMAPFolder)store.getFolder("Sent Messages"); //发件箱
			folder = (IMAPFolder) store.getFolder("INBOX"); // 收件箱

			Folder defaultFolder = store.getDefaultFolder();
			Folder[] allFolder = defaultFolder.list();
			for (int i = 0; i < allFolder.length; i++) {
				System.out.println("这个是服务器中的文件夹=" + allFolder[i].getFullName());
			}
			// 使用只读方式打开收件箱
			folder.open(Folder.READ_ONLY);
			int size = folder.getMessageCount();
			System.out.println("这里是打印的条数==" + size);
			Message[] message = folder.getMessages();
			// Message message = folder.getMessage(size);
			for (int i = 0; i < 3; i++) {
				System.out.println("------------------------------------");
				Address[] address = message[i].getFrom();
				for (int j = 0; j < address.length; ++j)
					System.out.println("发信人: " + address[j]);
				// String from = message[i].getFrom()[0].toString();
				String subject = message[i].getSubject();
				Date date = message[i].getSentDate();
				// System.out.println("发信人: " + from);
				System.out.println("主题: " + subject);
				System.out.println("日期: " + date);
				 // 解析邮件内容
                Object content = message[i].getContent(); 
                if (content instanceof MimeMultipart) {      
                    MimeMultipart multipart = (MimeMultipart) content;      
                    parseMultipart(multipart);      
                } 
                if (content != null) {
                	System.out.println(content.toString());
                }
				System.out.println("------------------------------------");
			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (folder != null) {
					folder.close(false);
				}
				if (store != null) {
					store.close();
				}
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		System.out.println("接收完毕！");
	}

	public static void main(String[] args) {
//		SendMail();
//		FetchMailImap();
//		ReceiveMail();
		MailBase mb = new MailBase("wpeng@rgprt.com", "wzbhbb67782155");
//		mb.FetchMailImap();
		mb.SendMail("wzbhbb@qq.com", "电票打印", "wzbhbb67782155wzbhbb67782155wzbhbb67782155wzbhbb67782155wzbhbb67782155", "");
	}

}
