package test1.demo.mail;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Security;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;

@SuppressWarnings("restriction")
public class MailBase {
	/**
	 * 邮件发送者的用户名和密码
	 */
	private transient String username = "";
	private transient String password = "";

	/**
	 * 发送邮件的props文件 (可使用自建priperty文件) 用于初始化一个session实例，配置了一个session会话的一些基本信息
	 */
	private final transient Properties props = new Properties();
	/**
	 * session为一个基本的邮件会话，通过该会话可执行其他邮件工作 如：之后的初始化一个MimeMessage实例
	 */
	private transient Session session;

	/**
	 * 发送邮件服务器地址
	 */
	private transient String smtpHost;
	private transient String smtpPort;

	/**
	 * 接收邮件服务器地址
	 */
	private transient String imapHost;
	private transient String imapPort;

	/**
	 * 邮件内容类型 （这里演示一个html格式的消息格式）
	 */
	private final static String CONTENT_TYPE_HTML = "text/html;charset=utf-8";

	/**
	 * 邮件使用SSL
	 */
	private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

	/**
	 * 初始化函数
	 * 
	 * @param mailName
	 *            用于接收或者发送的邮箱地址
	 * @param mailPass
	 *            用于接收或者发送的邮箱密码
	 */
	public MailBase(String mailName, String mailPass) {
		this.username = mailName;
		this.password = mailPass;

		/**
		 * 填入默认参数
		 */
		this.smtpHost = "smtp.exmail.qq.com";
		this.smtpPort = "465";
		this.imapHost = "imap.exmail.qq.com";
		this.imapPort = "993";

		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
	}

	/**
	 * 发送邮件
	 * 
	 * @param emailTo
	 *            收件人邮件地址
	 * @param subject
	 *            主题
	 * @param content
	 *            邮件内容
	 * @param attachment
	 *            附件路径，可以为空
	 */
	public void SendMail(String emailTo, String subject, String content, String attachment) {
		try {
			long st, se;
			st = System.currentTimeMillis();
			// 设置SSL连接、邮件环境
			Properties props = System.getProperties();
			props.setProperty("mail.smtp.host", smtpHost);
			props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
			props.setProperty("mail.smtp.socketFactory.fallback", "false");
			props.setProperty("mail.smtp.port", smtpPort);
			props.setProperty("mail.smtp.socketFactory.port", smtpPort);
			props.setProperty("mail.smtp.auth", "true");
			st = System.currentTimeMillis()-st;
			System.out.println("执行时间1:" + st);
			st = System.currentTimeMillis();
			// 建立邮件会话
			Session session = Session.getDefaultInstance(props, new Authenticator() {
				// 身份认证
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			st = System.currentTimeMillis()-st;
			System.out.println("执行时间2:" + st);
			st = System.currentTimeMillis();
			/**
			 * 建立邮件对象
			 */
			MimeMessage message = new MimeMessage(session);
			// 设置邮件的发件人、收件人、主题
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, emailTo);
			message.setSubject(subject);

			st = System.currentTimeMillis()-st;
			System.out.println("执行时间3:" + st);
			st = System.currentTimeMillis();
			// 文本部分
			MimeBodyPart textPart = new MimeBodyPart();
			textPart.setContent("<h3>" + content + "</h3>", CONTENT_TYPE_HTML);

			st = System.currentTimeMillis()-st;
			System.out.println("执行时间3_1:" + st);
			st = System.currentTimeMillis();
			// 图文整合，关联关系
			MimeMultipart mmp1 = new MimeMultipart();
			mmp1.addBodyPart(textPart);
			mmp1.setSubType("related");
			MimeBodyPart textImagePart = new MimeBodyPart();
			textImagePart.setContent(mmp1);

			st = System.currentTimeMillis()-st;
			System.out.println("执行时间3_2:" + st);
			st = System.currentTimeMillis();
			if (attachment != null && attachment != "") {
				// 附件部分
				MimeBodyPart attachmentPart = new MimeBodyPart();
				DataHandler dh = new DataHandler(new FileDataSource(attachment));// 文件路径
				String fileName = dh.getName();
				attachmentPart.setDataHandler(dh);
				attachmentPart.setFileName(fileName);
				// 图文和附件整合，复杂关系
				MimeMultipart mmp2 = new MimeMultipart();
				mmp2.addBodyPart(textImagePart);
				mmp2.addBodyPart(attachmentPart);
				mmp2.setSubType("mixed");
				// 将以上内容添加到邮件的内容中并确认
				message.setContent(mmp2);
			} else
				message.setContent(mmp1);
			message.saveChanges();

			st = System.currentTimeMillis()-st;
			System.out.println("执行时间4:" + st);
			st = System.currentTimeMillis();
			// 发送邮件
			Transport.send(message);
			st = System.currentTimeMillis()-st;
			System.out.println("执行时间5:" + st);
			st = System.currentTimeMillis();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 通过IMAP方式读取邮件
	 */
	public void FetchMailImap() {
		Properties props = System.getProperties();
		props.setProperty("mail.imap.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.imap.socketFactory.fallback", "false");
		props.setProperty("mail.imap.port", imapPort);
		props.setProperty("mail.imap.socketFactory.port", imapPort);
		props.setProperty("mail.imap.ssl.enable", "true");
		props.setProperty("mail.store.protocol", "imap");
		props.setProperty("mail.imap.host", imapHost);
		props.setProperty("mail.imap.auth.login.disable", "true");
		Session session = Session.getInstance(props);
		session.setDebug(false);
		IMAPFolder folder = null;
		IMAPStore store = null;
		try {
			store = (IMAPStore) session.getStore("imap"); // 使用imap会话机制，连接服务器
			store.connect(username, password);
			// folder=(IMAPFolder)store.getFolder("Sent Messages"); //发件箱
			folder = (IMAPFolder) store.getFolder("INBOX"); // 收件箱

			Folder defaultFolder = store.getDefaultFolder();
			Folder[] allFolder = defaultFolder.list();
			for (int i = 0; i < allFolder.length; i++) {
				System.out.println("服务器中的文件夹=" + allFolder[i].getFullName());
			}
			// 使用只读方式打开收件箱
			folder.open(Folder.READ_ONLY);
			int size = folder.getMessageCount();
			System.out.println("这里邮件条数==" + size);
			Message[] message = folder.getMessages();
			for (int i = 0; i < 2; i++) {
				System.out.println("------------------------------------");
				Address[] address = message[i].getFrom();
				for (int j = 0; j < address.length; ++j)
					System.out.println("发信人: " + address[j]);
				// String from = message[i].getFrom()[0].toString();
				String subject = message[i].getSubject();
				Date date = message[i].getSentDate();
				// System.out.println("发信人: " + from);
				System.out.println("主题: " + subject);
				System.out.println("日期: " + date);
				// 解析邮件内容
				Object content = message[i].getContent();
				if (content instanceof MimeMultipart) {
					MimeMultipart multipart = (MimeMultipart) content;
					ParseMultipart(multipart);
				}
				if (content != null) {
					System.out.println(content.toString());
				}
				System.out.println("------------------------------------");
			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (folder != null) {
					folder.close(false);
				}
				if (store != null) {
					store.close();
				}
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		System.out.println("接收完毕！");
	}

	/**
	 * POP3方式读取邮件，基本不用
	 */
	public void FetchMailPop3() {
		// 设置SSL连接、邮件环境
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = System.getProperties();
		props.setProperty("mail.pop3.host", "pop.exmail.qq.com");
		props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.pop3.socketFactory.fallback", "false");
		props.setProperty("mail.pop3.port", "995");
		props.setProperty("mail.pop3.socketFactory.port", "995");
		props.setProperty("mail.pop3.auth", "true");
		// 建立邮件会话
		Session session = Session.getDefaultInstance(props, null);

		System.out.println("step 1");
		// session.setDebug(true);
		// 设置连接邮件仓库的环境
		URLName url = new URLName("pop3", "pop.exmail.qq.com", 995, null, "wpeng@rgprt.com", "wzbhbb67782155");
		Store store = null;
		Folder inbox = null;
		try {
			// 得到邮件仓库并连接
			store = session.getStore(url);
			store.connect();
			// 得到收件箱并抓取邮件
			inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_WRITE); // 可以删除邮件

			int messageCount = inbox.getMessageCount();
			System.out.println(messageCount);
			Message[] messages = inbox.getMessages();

			// 打印收件箱邮件部分信息
			int length = messages.length;
			System.out.println("收件箱的邮件数：" + length);
			System.out.println("-------------------------------------------\n");
			for (int i = 0; i < length; i++) {
				String from = MimeUtility.decodeText(messages[i].getFrom()[0].toString());
				InternetAddress ia = new InternetAddress(from);
				System.out.println("发件人：" + ia.getPersonal() + '(' + ia.getAddress() + ')');
				System.out.println("主题：" + messages[i].getSubject());
				System.out.println("邮件大小：" + messages[i].getSize());
				System.out.println(
						"邮件发送时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(messages[i].getSentDate()));

				if ("Re:Re: (Zhaopin.com) 应聘 软件工程师-北京-李振鹏".equals(messages[i].getSubject())) {
					System.out.println("++++++++++++++++++++++++++++++找到了+++++++++++++++++++======");
					messages[i].setFlag(Flags.Flag.DELETED, true);
					break;
				}
				// 解析邮件内容
				Object content = messages[i].getContent();
				if (content instanceof MimeMultipart) {
					MimeMultipart multipart = (MimeMultipart) content;
					ParseMultipart(multipart);
				}
				System.out.println("-------------------------------------------\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inbox.close(true);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
			try {
				store.close();
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 对复杂邮件的解析
	 * 
	 * @param multipart
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void ParseMultipart(Multipart multipart) throws MessagingException, IOException {
		int count = multipart.getCount();
		for (int idx = 0; idx < count; idx++) {// 附件内容数量
			BodyPart bodyPart = multipart.getBodyPart(idx);
			System.out.println(bodyPart.getContentType());
			if (bodyPart.isMimeType("text/plain")) {
				System.out.println("plain................." + bodyPart.getContent());
			} else if (bodyPart.isMimeType("text/html")) {
				System.out.println("html..................." + bodyPart.getContent());
			} else if (bodyPart.isMimeType("multipart/*")) {
				Multipart mpart = (Multipart) bodyPart.getContent();
				ParseMultipart(mpart);
			} else if (bodyPart.isMimeType("application/octet-stream")) {
				String disposition = bodyPart.getDisposition();
				System.out.println("发现一个位置:" + disposition);
				if (disposition.equalsIgnoreCase(BodyPart.ATTACHMENT)) {
					try {
						// =?GB2312?B?1tzA/bvhvMfCvDIwMTQuNC4xNC5kb2M=?=
						String fullFileName = bodyPart.getFileName();

						// 分离编码方式
						String[] paras = fullFileName.split("\\?");// 1存放编码方式，
																	// 3存放名称
						String fn = new String(Base64.decode(paras[3].getBytes(paras[1])), paras[1]);
						System.out.println("发现一个附件路径: " + fn);
						InputStream is = bodyPart.getInputStream();
						CopyAttachToLocal(is, new FileOutputStream("E:\\CloudMusic\\" + fn));
					} catch (Base64DecodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * 文件拷贝，在用户进行附件下载的时候，可以把附件的InputStream传给用户进行下载
	 * 
	 * @param is
	 * @param os
	 * @throws IOException
	 */
	private void CopyAttachToLocal(InputStream is, OutputStream os) throws IOException {
		byte[] bytes = new byte[1024];
		int len = 0;
		while ((len = is.read(bytes)) != -1) {
			os.write(bytes, 0, len);
		}
		if (os != null)
			os.close();
		if (is != null)
			is.close();
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getImapHost() {
		return imapHost;
	}

	public void setImapHost(String imapHost) {
		this.imapHost = imapHost;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getImapPort() {
		return imapPort;
	}

	public void setImapPort(String imapPort) {
		this.imapPort = imapPort;
	}
}
